# Webpack + Bootstrap 4 boilerplate

Based on <https://github.com/Harrix/static-site-webpack-habr>

Template <https://getbootstrap.com/docs/4.0/examples/carousel/>

## cli

1. `npm run-script watch` -- run webpack watch mode
2. `npm run-script start` -- run webpack dev webserver
3. `npm run-script dev`   -- build project dev mode
4. `npm run-script build` -- build project production mode
